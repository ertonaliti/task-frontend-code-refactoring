let http = new XMLHttpRequest();
http.open('get', './data/data.json', true);
http.send();

http.onload = function (){
    if(this.readyState == 4 && this.status == 200){
        let objects = JSON.parse(this.responseText);
        let output = "";
        counter = 0;
        for(let item of objects){
            let date = item.date;
            let dateFormat = new Date(date);
            let year = dateFormat.getFullYear();
            let month = dateFormat.toLocaleString('default', { month: 'short' });;
            let day = dateFormat.getDate();
            let newDateFormat = day + " " + month + " " + year;
            output +=   `
                            <div id="card${counter}" class="card" data-name="p-${counter}">
                                <div class="header">
                                    <div class="header-profile">
                                        <div class="profile-img">
                                            <img id="profile-${counter}" src="${item.profile_image}" width="50px"/>
                                        </div>
                                        <div class="profile-text">
                                            <div class="profile-name">
                                                <p id="profile-name-${counter}">${item.name}</p>
                                            </div>
                                            <p id="profile-date-${counter}" class="profile-date">${newDateFormat}</p>
                                        </div>
                                    </div>
                                    <div class="header-logo">
                                        <img src="./images/instagram-logo.svg"/>
                                    </div>
                                </div>
                                <div class="body">
                                    <div id="body-image" class="body-image">
                                        <img class="img" id="${counter}" onclick="fullImage(this.id)" src="${item.image}"/>
                                    </div>
                                    <div class="body-caption">
                                        <p id="caption-${counter}">${item.caption}</p>
                                    </div>
                                </div>
                                <hr class="divide">
                                <div class="footer">
                                    <svg class="heartButton" viewBox="0 0 17 17"  onclick="toggleHeart(this)" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M14.7617 3.26543C14.3999 2.90347 13.9703 2.61634 13.4976 2.42045C13.0248 2.22455 12.518 2.12372 12.0063 2.12372C11.4945 2.12372 10.9878 2.22455 10.515 2.42045C10.0422 2.61634 9.61263 2.90347 9.25085 3.26543L8.50001 4.01626L7.74918 3.26543C7.0184 2.53465 6.02725 2.1241 4.99376 2.1241C3.96028 2.1241 2.96913 2.53465 2.23835 3.26543C1.50756 3.99621 1.09702 4.98736 1.09702 6.02084C1.09702 7.05433 1.50756 8.04548 2.23835 8.77626L2.98918 9.52709L8.50001 15.0379L14.0108 9.52709L14.7617 8.77626C15.1236 8.41448 15.4108 7.98492 15.6067 7.51214C15.8026 7.03935 15.9034 6.53261 15.9034 6.02084C15.9034 5.50908 15.8026 5.00233 15.6067 4.52955C15.4108 4.05677 15.1236 3.62721 14.7617 3.26543V3.26543Z" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
                                    </svg>
                                    <p id="counter-toggle-${counter}" class="counter-toggle" value=${item.likes}>${item.likes}</p>
                                </div>
                            </div>
                        `;
                        counter++;
        }       
        document.querySelector(".container").innerHTML = output;
    }
}

function toggleHeart(aTag) {

    var content = aTag.parentNode.getElementsByClassName("heartButton")[0];
    var counter = aTag.parentNode.getElementsByClassName("counter-toggle")[0];
    if(content.style.fill == null || content.style.fill == "red")
    {
        content.style.fill = 'none';
        counter.innerHTML = parseInt(counter.innerHTML)-1;
    }
    else
    {
        content.style.fill = 'red';
        counter.innerHTML = parseInt(counter.innerHTML)+1;
    }
}

// const lightboxBg = document.createElement("div");
// lightboxBg.id = "lightboxBg";

// document.body.appendChild(lightboxBg);

// function fullImage(clicked_id){

//         lightboxBg.setAttribute("data-target", clicked_id);
//         lightboxBg.setAttribute("class", "lightboxBg-active");

//         const lightboxImg = document.createElement('img');
//         lightboxImg.src = document.getElementById(clicked_id).value = event.target.src;
//         lightboxImg.id = "activeImg";
//         const div = document.createElement('div');
//         div.id = "img";
//         const secondDiv = document.createElement('div');
//         const profileImg = document.createElement('img');
//         profileImg.src = document.querySelector("#profile-"+clicked_id).src;
//         const secondDivChild = document.createElement('div');
//         console.log(profileImg);




//         let element = document.getElementById("lightboxBg");
//         while (element.firstChild) {
//             element.removeChild(element.firstChild);
//         }
//         lightboxBg.appendChild(div);
//         lightboxBg.appendChild(secondDiv);
//         div.appendChild(lightboxImg);
//         secondDiv.appendChild(secondDivChild);
//         secondDivChild.appendChild(profileImg);
// }

var popupContainer= document.querySelector('.popup-container');
popupContainer.addEventListener('click', e=>{
    if (event.target == popupContainer) {
        popupContainer.classList.remove('active');
      }
})

function fullImage(clicked_id){
    var popupContainer= document.querySelector('.popup-container');
    popupContainer.classList.add('active');

    var cardImageDiv = document.querySelector('.popup-card-image');
    var cardImage = document.createElement('img');
    cardImage.setAttribute('class', 'activeImg');

    var profileImgDiv = document.querySelector('.popup-profile-img');
    var profileImg = document.createElement('img');

    cardImage.src = document.getElementById(clicked_id).value = event.target.src;
    profileImg.src = document.querySelector('#profile-'+clicked_id).src;


    var profileNameDiv = document.querySelector('.popup-profile-name');
    var cardProfileName = document.createElement('p');
    var getProfileName = document.getElementById('profile-name-'+clicked_id).innerHTML;
    cardProfileName.textContent = getProfileName;


    var profileDateDiv = document.querySelector('.popup-profile-date');
    var cardProfileDate = document.createElement('p');
    var getProfileDate = document.getElementById('profile-date-'+clicked_id).innerHTML;
    cardProfileDate.textContent = getProfileDate;
    
    var profileHeaderLogo = document.querySelector('.popup-header-logo');
    var cardProfileHeaderLogo = document.createElement('img');
    cardProfileHeaderLogo.src = "./images/instagram-logo.svg";

    var popUpFooter = document.querySelector('.popup-footer-heart');
    var cardPopUpFooter = document.createElement('img');
    cardPopUpFooter.src = "./images/heart.svg";
    cardPopUpFooter.classList.add('active');


    var counterToggle = document.querySelector('#counter-toggle-'+clicked_id).innerHTML;
    var counterToggleDiv = document.querySelector('.popup-footer-counter');
    var cardCounterToggle = document.createElement('p');
    cardCounterToggle.textContent = counterToggle;
    cardCounterToggle.classList.add('counter-toggle');

    var popupCaptionDiv = document.querySelector('.popup-caption');
    var caption = document.querySelector('#caption-'+clicked_id).innerHTML;
    var popupCaption = document.createElement('p');
    popupCaption.textContent = caption;
    
    while(cardImageDiv.firstChild){
        cardImageDiv.removeChild(cardImageDiv.firstChild); 
    }
    while(profileImgDiv.firstChild){
        profileImgDiv.removeChild(profileImgDiv.firstChild); 
    }
    while(profileNameDiv.firstChild){
        profileNameDiv.removeChild(profileNameDiv.firstChild); 
    }
    while(profileDateDiv.firstChild){
        profileDateDiv.removeChild(profileDateDiv.firstChild); 
    }
    while(profileHeaderLogo.firstChild){
        profileHeaderLogo.removeChild(profileHeaderLogo.firstChild); 
    }
    while(popUpFooter.firstChild){
        popUpFooter.removeChild(popUpFooter.firstChild); 
    }
    while(counterToggleDiv.firstChild){
        counterToggleDiv.removeChild(counterToggleDiv.firstChild); 
    }

    while(popupCaptionDiv.firstChild){
        popupCaptionDiv.removeChild(popupCaptionDiv.firstChild); 
    }
    
    cardImageDiv.appendChild(cardImage);
    profileImgDiv.appendChild(profileImg);
    profileNameDiv.appendChild(cardProfileName);
    profileDateDiv.appendChild(cardProfileDate);
    profileHeaderLogo.appendChild(cardProfileHeaderLogo);
    popUpFooter.appendChild(cardPopUpFooter);
    counterToggleDiv.appendChild(cardCounterToggle);
    popupCaptionDiv.appendChild(popupCaption)
    
}

// const mediaQuery = window.matchMedia('(max-width: 992px)');
// if (mediaQuery.matches) {
//     console.log(document.querySelector('.img'));
// }

var currentItem = 4;
function loadMoreFunction(){

    var cards = document.querySelectorAll(".card");
    var btn = document.querySelector("#loadBtn");
        for(var i = currentItem; i < currentItem + 4; i++ ){
            if(cards[i] ){
                cards[i].style.display = 'block';
            }
        }
    currentItem += 4;
    var container = document.getElementById("container").children.length;

        if(currentItem == container){
            btn.style.display = 'none';
        }else{
            btn.style.display = 'flex';
        }
}